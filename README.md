![Hexo Version](https://img.shields.io/badge/Hexo-3.2.0-blue.svg)
![Node Version](https://img.shields.io/badge/NodeJS-4.2.2-green.svg)
![Build Status](http://gitlab.com/ramosmd/hexo/badges/master/build.svg)

----

View site: https://ramosmd.gitlab.io/hexo
Custom Domain: https://ramosmd.ml

----

# Build an [Hexo] site with GitLab Pages

## Settings

- Theme: [Heuman]
- GitLab CI - view [`.gitlab-ci.yml`]
- Hexo - view [`_config.yml`]
- Package - view [`package.json`]

## Requirements

[Hexo] is a [SSG] powered by [NodeJS][node]. So we'll need this first:

- Node JS environment 
- [npm] (package manager)
- [git] core

## Script

We already have a Node image on [Docker Library][docker-lib], so we'll require it to run our script on the 
environment of NodeJS. 

This will bring `npm` and `git` core with it: `image: node:4.2.2`

Then we'll need to run:

<br>

```
$ npm install -g hexo-cli
$ npm install
$ hexo generate
```
<br>

We'll set the the `npm` modules and its dependecies as a `cache` to speed up our builds:

<br>

```yaml
cache:
  paths: 
    - node_modules/
```

### GitLab-CI

We need to prepare our `gitlab-ci.yml` configuration file to do as we described previously.

We'll also need a `pages` job and finally a `public` folder with our `artifacts`, afecting only our `master` branch:

<br>

```yaml
# requiring the environment of NodeJS 4.2.2
image: node:4.2.2

# add 'node_modules' to cache for speeding up builds
cache:
  paths: 
    - node_modules/ # Node modules and dependencies

before_script:
  - npm install -g hexo-cli # Install Hexo itself
  - npm install # Install Hexo modules and dependencies

# the 'pages' job will deploy and build your site to the 'public' path
pages:
  stage: deploy
  script:
    - hexo generate # Hexo generate Static Site
  artifacts:
    paths:
      - public 
  only:
    - master # this job will affect only the 'master' branch
```

### Testing our builds before pushing content to the `master` branch

What if we want to test our builds in another branch before afecting `master`? 
We'll just need to set a `test` job to do this for us:

<br>

```yaml
image: node:4.2.2

cache:
  paths: 
    - node_modules/ 

before_script:
  - npm install -g hexo-cli 
  - npm install 

# add a job called 'test'
test:
  stage: test
  script:
    - hexo generate
  except:
    - master # the 'test' job will affect all branches expect 'master'

# the 'pages' job will deploy and build your site to the 'public' path
pages:
  stage: deploy
  script:
    - hexo generate
  artifacts:
    paths:
      - public 
  only:
    - master # this job will affect only the 'master' branch
```

----

## Usage

_**Note:** we assume you have [Hexo 3][Hexo] installed, up and running locally._

- Fork, clone or download this project
- Install [Hexo Server][server] if you don't have it already: `npm install hexo-server --save`
- Configure Hexo [`_config.yml`] according to your project
- Navigate to the project folder (`cd path/to/project`)
- Run `hexo server`

### How to change Hexo version?

Modify [`package.json`]:

<br>

```json
{
  "hexo": {
    "version": "3.2.0"
  }
}
```

----

## References

- NodeJS [Docs][node-docs]
- npm [Docs][npm-docs]
- Hexo [Docs][hexo-docs]
- Hexo [Themes]

----

## Enjoy!


[docker-lib]: https://hub.docker.com/u/library/
[Hexo]: https://hexo.io/
[Heuman]:  https://github.com/ppoffice/hexo-theme-hueman
[hexo-docs]: https://hexo.io/docs/
[node]: http://nodejs.org/
[node-docs]: https://nodejs.org/en/docs/
[git]: http://git-scm.com/
[npm]: https://www.npmjs.com/
[npm-docs]: https://docs.npmjs.com/
[server]: https://hexo.io/docs/server.html#hexo-server
[SSG]: https://www.staticgen.com/
[Themes]: https://hexo.io/themes/

[`_config.yml`]: https://gitlab.com/ramosmd/hexo/blob/master/_config.yml
[`.gitlab-ci.yml`]: https://gitlab.com/ramosmd/hexo/blob/master/_config.yml
[`package.json`]: https://gitlab.com/ramosmd/hexo/blob/master/package.json

